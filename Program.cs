﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Threading;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program instance = new Program();
            Console.WriteLine(instance.GetTotalTax(10, 500, 5));
            Console.WriteLine(instance.GetCongratulation(12));
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            string str1 = "123,456";
            double first;
            var resultFirst = double.TryParse(str1, out first);
            if (!resultFirst)
            {
                Console.WriteLine("Неправильный формат ввода");
            }
            else
            {
                string str2 = "1000";
                double second;
                var resultSecond = double.TryParse(str2, out second);
                if (!resultSecond)
                {
                    Console.WriteLine("Неправильный формат ввода");
                }
                else
                {
                    Console.WriteLine(instance.GetMultipliedNumbers(str1, str2));
                }
            }
            Dimensions dimensions = new Dimensions { FirstSide = 38, SecondSide = 36, ThirdSide = 17, Height = 0, Diameter = 50, Radius = 25 };
            Console.WriteLine(instance.GetFigureValues(Figure.Triangle, Parameter.Square, dimensions));
            Console.ReadKey();
        }
        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            int sumTax = companiesNumber * companyRevenue * tax / 100;
            return sumTax;
        }
        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18)
            {
                return "Поздравляю с совершеннолетием!";
            }
            else if ((input % 2 != 0) && input < 18 && input > 12)
            {
                return "Поздравляю с переходом в старшую школу!";
            }
            else
            {
                return "Поздравляю с " + input + "-летием!";
            }
        }
        public double GetMultipliedNumbers(string first, string second)
        {
            double value1 = double.Parse(first);
            double value2 = double.Parse(second);
            return value1 * value2;
        }
        public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            if (figureType == Figure.Triangle)
            {
                if (parameterToCompute == Parameter.Square)
                {
                    if (dimensions.Height == 0)
                    {
                        double firstSide = dimensions.FirstSide;
                        double secondSide = dimensions.SecondSide;
                        double thirdSide = dimensions.ThirdSide;
                        double perimeter = firstSide + secondSide + thirdSide;
                        double p = 0.5 * perimeter;
                        double square = Math.Sqrt(p * (p - firstSide) * (p - secondSide) * (p - thirdSide)) - 1;
                        return Math.Round(square);
                    }
                    else
                    {
                        double firstSide = dimensions.FirstSide;
                        double height = dimensions.Height;
                        double square = (firstSide * height) / 2;
                        return Math.Round(square);
                    }
                }
                else
                {
                    double firstSide = dimensions.FirstSide;
                    double secondSide = dimensions.SecondSide;
                    double thirdSide = dimensions.ThirdSide;
                    double perimeter = firstSide + secondSide + thirdSide;
                    return Math.Round(perimeter);
                }
            }
            else if (figureType == Figure.Rectangle)
            {
                if (parameterToCompute == Parameter.Square)
                {
                    double firstSide = dimensions.FirstSide;
                    double secondSide = dimensions.SecondSide;
                    double square = firstSide * secondSide;
                    return Math.Round(square);
                }
                else
                {
                    double firstSide = dimensions.FirstSide;
                    double secondSide = dimensions.SecondSide;
                    double perimeter = (firstSide + secondSide) * 2;
                    return Math.Round(perimeter);
                }
            }
            else
            {
                if (parameterToCompute == Parameter.Square)
                {
                    double radius = dimensions.Radius;
                    double square = Math.PI * Math.Pow(radius, 2);
                    return Math.Round(square);
                }
                else
                {
                    double radius = dimensions.Radius;
                    double perimeter = 2 * Math.PI * radius;
                    return Math.Round(perimeter);
                }
            }
        }
    }
}
